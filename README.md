# HTML Template

My personal HTML5 boilerplate

> This project assumes that you have basic knowledge of HTML5

---

There are more than 1 ways to write a HTML boilerplate,
so i explain my decisions when writing this boilerplate:

### Formatting

- Uses indents to make clear the structure
- Every indent is 2 spaces
- The major sections (Header, Body) are separated by 2 empty lines
- Sections inside the major sections (Header, Body) are separated by 1 empty line

### ``<html>`` tag

- The language in the ``<html>`` tag is set to English
  since it's "The" internet language

### Header (``<head>``) section

- The character set (charset) is set to UTF-8
  since it's the standard character set on English websites
- The charset is set Before the tab title,
  so that even the tab title (instead of just the Body) gets displayed correctly
  (web browsers read HTML from up to down, and read everything only once)
- The tab title is set to ``Tab Title``
  so that you can find more easily where to change it
- The ``<link rel="stylesheet">`` tag links to a stylesheet with the filename ``index.css``
  so that when you create a stylesheet with the filename ``index.css`` in the same folder as the webpage
  it instantly gets applied (after reloading the page, of course)
- The ``<meta name="viewport">`` tag in this boilerplate
  is used by almost all modern websites
  to tell mobile browsers to use native scaling
  instead of scaling it like a desktop website.

### Body (``<body>``) section

- I put a Heading (``<h1>``) and Paragraph (``<p>``)
  since most websites start out with these elements.
  These have ``Heading 1`` and ``Paragraph`` respectively
  as their default text to reenforce what they are.
- The ``<script>`` tag references a JavaScript file named ``index.js``
  so that when you create it in the same folder as the webpage
  it instantly gets applied (after reloading, of course).
  I put it at the very end of the page so that the script won't have trouble with elements it references not having been loaded yet.
  